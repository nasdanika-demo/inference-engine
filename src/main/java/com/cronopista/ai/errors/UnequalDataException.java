package com.cronopista.ai.errors;

public class UnequalDataException extends Exception {

	private static final long serialVersionUID = 1L;

	public UnequalDataException(String message) {
		super(message);
	}

}
